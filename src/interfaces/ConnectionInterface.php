<?php

namespace App\Interfaces;

interface ConnectionInterface
{
    public function connect(string $server, string $user, string $password, string $database);
}