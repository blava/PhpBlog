<?php

namespace App\controllers;

class MyController {
    protected  $twig;

    public function __construct($twig) {
        $this->twig = $twig;
    }

    public function somefunc($params = []) {
        echo $this->twig->render('index.html.twig', $params);
    }

    public function aboutMe($params = []) {
        echo $this->twig->render('about.html.twig', $params);
    }
}