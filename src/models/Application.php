<?php

namespace App\models;

use App\controllers\MyController;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Application
{
    public function __construct()
    {
        $this->router = new Router();
        $loader = new FilesystemLoader( str_replace( 'models', '', __DIR__). 'views');
        $twig = new Environment($loader);
        $this->myController = new MyController($twig);
    }

    public function run()
    {
        $this->router->get('public/about', $this->myController->aboutMe());
        $this->router->get('public', $this->myController->somefunc(['name' => 'Mikheil', 'about' => 'lorem ipsum'] ));
        $this->router->run();
    }
}