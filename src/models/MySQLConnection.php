<?php

namespace App\models;

use Exception;
use interfaces\ConnectionInterface;

class MySQLConnection implements ConnectionInterface {

public function connect(string $server, string $user, string $password, string $database)
{
    $dbh = new PDO('mysql:host=' . $server . ';dbname=' . $database, $user, $password);
    if (!$dbh)
        throw new Exception("Can't connect to the database");
    }
}
//SGUmcPUTBwEDD7bv