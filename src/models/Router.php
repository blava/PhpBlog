<?php

namespace App\models;

class Router
{
    protected  array $handlers =[];

    public function get(string $uri, $handler) {
        $this->handlers['GET' . $uri] = [
                'uri' => $uri,
                'method' => 'GET',
                'handler' => $handler
        ];
    }

    public function post(string $uri, $handler) {
        $this->handlers['POST' . $uri] = [
            'uri' => $uri,
            'method' => 'POST',
            'handler' => $handler
        ];
    }

    public function run() {
        $uri = parse_url($_SERVER['REQUEST_URI']);
        $path = $uri['host'] . $uri['path'];
        $method = $_SERVER['REQUEST_METHOD'];
        $callback = null;
//        var_dump($path);
        foreach ($this->handlers as $handler) {
         if ($handler['uri'] == $path && $handler['method'] == $method) {
             $callback = $handler['handler'];
             break;
            }
        }
        call_user_func_array($callback, array_merge($_GET, $_POST));
    }
}